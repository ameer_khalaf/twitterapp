
//var app = module.exports.app = exports.app = express();

// set up ========================
var express  = require('express');
var app      = express();                               // create our app w/ express
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var Twitter = require('twitter'); // twitter plugin
var config = require('./config.js'); // store app access keys

// configuration =================

app.use(express.static(__dirname + '/build'));                 // set the static files location /public/img will be /img for users
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(methodOverride());

// twitter config ================
var client = new Twitter(config);

//client.get('search/tweets', { q: 'banana since:2011-07-11', count: 100 }, function(err, data, response) {
//    console.log(data);
//});
app.get('/app/homeTimeline', function(req, res, next) {
    client.get('statuses/home_timeline', function(error, tweets, response){
        if(error) throw error;
        res.send(tweets);
    });
});

//client.get('statuses/home_timeline', {count: 2}, function(err, data, response) {
//    console.log(data);
//});

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");