/**
 * search wiki for data using wiki public api
 */
(function () {
    "use strict";
    function wikiSearch($q, $http) {
        var api_base = "https://en.wikipedia.org/w/api.php?";
        var fullParams = "format=json&action=query&generator=search&gsrnamespace=0&gsrlimit=1&prop=extracts&exintro&explaintext&exsentences=2&exlimit=1&gsrsearch=";
        var callback = "&callback=JSON_CALLBACK";

        /**
         * run a wiki api query for search string
         * @param str
         * @returns {*}
         */
        function search(queryStr) {
            var deferred = $q.defer();
            var result = "";
            var query = api_base + fullParams + encodeURIComponent(queryStr) + callback;
            $http.jsonp(query)
                .success(function (data) {
                    if (data && data.query && data.query.pages) {
                        var pages = data.query.pages;
                        result = pages[Object.keys(pages)[0]].extract;
                    } else {
                        // handle no result
                        result = "No valid wiki article.";
                    }


                    deferred.resolve(result);
                });
            return deferred.promise;

        }

        return {
            search: search
        };
    }

    angular.module("TwitterApp").service("wikiService", ["$q", "$http", wikiSearch]);
}());
