(function () {
    "use strict";

    function wikiTootltip($compile, $window, $document, wikiService) {
        return {
            restrict: "A",
            scope: {},
            link: function (scope, element, attr) {

                scope.dynamicText = "Wiki";
                scope.tooltipIsOpen = true;
                var clickedOutsite = false;
                var clickedElement = false;

                var template = '<span class="toolTip" uib-tooltip="{{dynamicText}}" tooltip-is-open="tooltipIsOpen" tooltip-placement="top"></span>';
                var toolTip = $compile(template)(scope);
                element.append(toolTip);

                function moveTooltip(event) {
                    var y = event.pageY;
                    var x = event.pageX;
                    element.find('span.toolTip').css({
                        position: 'absolute',
                        top: y + 'px',
                        left: x + 'px'
                    });
                }

                function clickedOutside(event) {
                    console.log("event", event);
                    clickedOutsite = true;
                    if (clickedOutsite && !clickedElement) {
                        scope.tooltipIsOpen = false;
                        scope.$apply();
                    }
                }
                function clickedInside() {
                    clickedElement = true;
                    clickedOutsite = false;
                }

                function getSelectedText() {
                    var text = "";
                    if (typeof $window.getSelection !== "undefined") {
                        text = $window.getSelection().toString();
                    } else if (typeof $document.selection !== "undefined" && $document.selection.type === "Text") {
                        text = $document.selection.createRange().text;
                    }
                    return text;
                }

                function onTextSelected(event) {
                    clickedElement = true;
                    console.log("onTextSelected");
                    var selectedText = getSelectedText();
                    if (selectedText) {
                        moveTooltip(event.originalEvent);
                        console.log(selectedText);
                        wikiService.search(selectedText)
                            .then(function (data) {
                                console.log(data);
                                // scope.$apply(function(){
                                scope.dynamicText = data;
                                scope.tooltipIsOpen = true;
                                clickedElement = false;
                                // });
                            });

                    } else {
                        scope.tooltipIsOpen = false;
                        scope.$apply();
                    }
                }
                /****** bind events ******/

                element.bind('mousedown', clickedInside);
                element.bind('mouseup', onTextSelected);
                element.bind('keyup', onTextSelected);
                $window.onmouseup = clickedOutside;
            }
        };
    }

    angular.module("TwitterApp").directive("wikiTooltip", wikiTootltip);
}());
