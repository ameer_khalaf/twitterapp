angular.module("TwitterApp", [ "ngAnimate", "ui.bootstrap", "ui.router", "ngSanitize"]);

angular.module("TwitterApp").config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/twitter_feed');

        $stateProvider
            .state('twitter_feed', {
                url: '/twitter_feed',
                templateUrl: 'twitter/twitters.html',
                controller:'TwitterController'
            });
    });