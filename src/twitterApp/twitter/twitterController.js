
function twitterController($scope, twitterService){
    $scope.twitterFeed = ["ameer"];

    function init(){
        twitterService.homeTimelineFeed()
            .then(function(data){
                $scope.twitterFeed = data;
                console.log(data);
        });
    }

    function refreshTwitterFeed(){
        $scope.twitterFeed=[];
        twitterService.homeTimelineFeed()
            .then(function(data){
                $scope.twitterFeed = data;
            });
    }
    /******** $scope mapping * **********/
    $scope.refreshTwitterFeed = refreshTwitterFeed;
    init();

}
angular.module("TwitterApp").controller("TwitterController",["$scope", "twitterService", twitterController]);