(function() {
    "use strict";
    function tweetController() {
        var ctrl = this;

        ctrl.convertToTimeStamp = function (dateStr) {
            var result = new Date(dateStr);
            return result.getTime();
        };
    }

    angular.module("TwitterApp").component('tweetDetails', {
        templateUrl: 'twitter/tweetComponent.html',
        controller: tweetController,
        bindings: {
            tweet: "="
        }
    });
}());