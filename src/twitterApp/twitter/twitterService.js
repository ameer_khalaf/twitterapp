function twitterService($rootScope, $http, $q){

    function homeTimelineFeed(){
        var deferred = $q.defer();
        // make the call to backend
        $http.get('app/homeTimeline').success(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    }

    return{
        homeTimelineFeed : homeTimelineFeed
    };
}
angular.module("TwitterApp").service("twitterService",["$rootScope", "$http", "$q", twitterService]);