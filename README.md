
# Twitter Feed App 
Single page application to display a feed of tweets. When a user selects any word from any tweet,

a tooltip will display the first paragraph of the corresponding Wikipedia article.

Twitter API: https://dev.twitter.com/overview/documentation

Wikipedia API: http://en.wikipedia.org/w/api.php

### Single Page Application framework

This framework exists to enable rapid development of single page application which leverage the [AngularJS](https://angularjs.org/)
client-side web application framework, [nodejs.org](http://nodejs.org), Utilising [gulp](https://github.com/gulpjs/gulp), the streaming build system, and various third party plugins,
this framework intends to ensure consistency of front end code styling and structure.


## Installation

1. Download node at [nodejs.org](http://nodejs.org) and install it, if you haven't already.
2. Install gulp
    
        # On Windows, with administrator rights
        npm install -g gulp
        
        # On linux/mac
        sudo npm install -g gulp 
    
3. Check out this repository and install supporting node packages
    
        git clone https://ameer_khalaf@bitbucket.org/ameer_khalaf/twitterapp.git twitterApp
        cd ./twitterApp
        npm install
        
4. Add required user to set  Twitter access credentials:  
    1.  copy config-empty.js to config.js 
    2. open the file config.js in the project root 
    3. add your twitter access details as required 
    4. save changes 

5. Run the application 
```
       > gulp develop ```
6. open a web browser and go to  http://localhost:8080 

## Framework Usage

The gulp file is configured with three primary tasks: build, and develop.
Each of these tasks are depend on a number of secondary tasks to achieve their outcomes 
(i.e. cleaning project artifacts, compiling stylesheets, linting sources).

### Developing
To begin developing, call the develop task from the command line (using ```gulp develop```).

This task will compile the single page application, as well as all related components, launch a web server
(accessible from <http://localhost:8080/>), and watch for any changes to the source files which require applications or
components to be recompiled. Compiled content is served from the build task's output directory.
Supporting content (i.e. third-party libraries and static resources) is served from the source root directory.


 
### Building
The build task is used to produce standalone single page application form HTML pages and related components.
The gulpfile is configured such that a failure of any task along this process will cause the build task to discard the file,
and subsequent tasks in the chain will not be called (e.g. if JSHint fails to lint a file, the devutils task will not be called).

To begin, the LESS task will compile any component or application LESS files into CSS files of the same name alongside the LESS files.
Next, all component and application JavaScript files will be run through JSHint to ensure consistent code styling and structure.

Finally, application pages and related components will be compiled and written out into the build directory.
Application pages are constructed inside the skeleton HTML document (currently index.html)
and writing the newly standalone page to the build task's output directory. Related components are then constructed into
standalone modules by merging all scripts and styles in the module component script and style files  into singular 
concatenated files, excluding descendant modules. The same process is repeated for any descendant submodules 
HTML files contained in the module or its descendant modules referenced by an angular directive's templateUrl property 
are automatically assumed to be module templates/partials, and are copied into their directive's expected destination.


### Editor Config
When developing on this project you must use the [editorconfig](http://editorconfig.org/) in the project to keep the code consistant and clean.

[SublimeText Setup](https://github.com/sindresorhus/editorconfig-sublime#readme) Install [Package Control](https://packagecontrol.io/) first to install editorconfig for sublime text.



