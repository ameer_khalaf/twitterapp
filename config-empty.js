// Add your keys
// Rename file to config.js
// make sure config.js is add to .gitignore

module.exports = {
    consumer_key:         '',
    consumer_secret:      '',
    access_token:         '',
    access_token_secret: ''
}