var gulp = require("gulp"),
    fs = require("fs"),
    del = require("del"),
    bower = require("main-bower-files"),
    merge = require("merge-stream"),
    less = require("gulp-less"),
    runSequence = require('run-sequence'),
    gls = require('gulp-live-server'),
    plugins = require("gulp-load-plugins")(),
    inject = require('gulp-inject');

// Add gulp-help wrapping
require("gulp-help")(gulp);
// store the target app name
var targetApp = 'twitterApp';


// Clean existing compiled sources "Wipe target destinations before build if they exist"
gulp.task("clean-build",  function() {
    return del(["build"]);
});





// "build the package and set the server",
gulp.task("develop",  function(callback) {
    runSequence("build", "server");
});


// Run the build process without watching and serving
// TODO: implement support for multiple app build
// "Primary task for compiling source into built code",
gulp.task("build", function(callback) {
    runSequence("clean-build" , "build:less", "build:app", "build:index", "moveStatic", callback);
});

// Compile less files in-place into CSS files
gulp.task("build:less", false, function() {
   return gulp.src('**/*.less',{ cwd: 'src/' + targetApp})
        .pipe(less())
        .pipe(gulp.dest('build/'));
});

gulp.task("build:app",["build:less"], function() {

    // capture application directories
    // move application html template files

    var appFilesHTML = gulp.src(['**/*.html', '!**/*.js'], {cwd: 'src/' + targetApp})
        .pipe(gulp.dest('build'));
    // contact and move js files into app.js
    var appFilesJs = gulp.src(['app.js', '**/*.js', '!**/*.test.js',  '!**/*.spec.js'], {cwd: 'src/' + targetApp})
        .pipe(gulp.dest('build'));

    return merge(appFilesHTML, appFilesJs);

    //TODO: add lintJS Task, add unit test task.

});

// move static resource to build directory e.g libs
gulp.task("moveStatic", function(){
    return gulp.src(['lib/**/*'],{cwd:'src'})
        .pipe(gulp.dest('build/lib'));
});
gulp.task("build:index",["build:app"], function(){

    /* TODO: add angualr file sort "gulp-angular-filesort" to the inject process recommanded by https://www.npmjs.com/package/gulp-inject */
    var target = gulp.src('build/index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['**/*.js', '**/*.css', '!lib'], {
        read: false,
        cwd: __dirname + '/build'
    }, {ignorePath: '/build'});

    target.pipe(plugins.inject(sources))
        .pipe(plugins.inject(gulp.src(bower(), {
            read: false,
            cwd: __dirname + '/src'
        }), {
            addRootSlash: false,
            name: "bower",
            endtag: "<!-- endbower -->"
        }))
        .pipe(gulp.dest('build'));

    return target;
});


gulp.task("server",function(){
    var server = gls.new('server.js');
    server.start();
});
